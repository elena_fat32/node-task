const http = require('http');
const fs = require('fs');
const routing = require('./routing');
var express = require('express');
var path = require('path');

let server = new http.Server(function(req, res) {
    var jsonString = '';

    if (req.url.indexOf('.js') != -1) {
        fs.readFile(__dirname + '/public/js/script.js', function (err, data) {
            if (err) console.log(err);

            res.writeHead(200, {'Content-Type': 'text/javascript'});
            res.end(data);
        });
    }

    if (req.url.indexOf('.css') != -1) {
        fs.readFile(__dirname + '/public/css/style.css', function (err, data) {
            if (err) console.log(err);

            res.writeHead(200, {'Content-Type': 'text/css'});
            res.end(data);
        });
    }

    req.on('data', (data) => {
        jsonString += data;
    });

    req.on('end', () => {
        routing.define(req, res, jsonString);
    });
});

server.listen(8000, 'localhost');
