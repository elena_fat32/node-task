const url = require('url');
const fs = require('fs');
var dynamic = require('./api');

const define = function (req, res, postData) {
    const urlParsed = url.parse(req.url, true);
    let path = urlParsed.pathname;

    prePath = __dirname;
    try {
        dynamic.getApi(req, res);
    } catch (err) {
        let nopath = './routing/nopage/index.html';

        fs.readFile(nopath, (err , html) => {
            if (!err) {
                res.writeHead(404, {'Content-Type': 'text/html'});
                res.end(html);
            } else{
                let text = "Something went wrong. Please contact webmaster";
                res.writeHead(404, {'Content-Type': 'text/plain'});
                res.end(text);
            }
        });
    }
};

exports.define = define;