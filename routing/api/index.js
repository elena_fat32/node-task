const url = require('url');
const fs = require('fs');
var swig = require('swig');
var parse = require('xml-parser');
var request = require('request');
var apiUrl = require('./apiUrl');

const getApi = function (req, res) {
    request.get(apiUrl.album, function (err, result) {
        if (err) throw err;

        var jsonObj = parse(result.body);

        var data = swig.renderFile(__dirname + "/index.html", {
            photos: parseJsonForView(jsonObj.root.children)
        });

        res.writeHead(200, {'Content-Type': 'text/html'});
        res.end(data);
    });
};

function parseJsonForView(obj) {
    var arrayData = [];

    for (var i = 0; i < obj.length; i++) {
        if (obj[i].name == 'entry') {
            var photoAttributesObj = {};
            var photoObjects = obj[i].children;

            for (var j = 0; j < photoObjects.length; j++) {
                var photoObject = photoObjects[j];

                if (photoObject.name == 'title') {
                    photoAttributesObj.title = photoObject.content;
                }

                if (photoObject.name == 'id') {
                    photoAttributesObj.id = photoObject.content;
                }

                if (photoObject.name == 'summary') {
                    photoAttributesObj.summary = photoObject.content;
                }

                if (photoObject.name == 'link' && photoObject.attributes.rel == 'edit-media') {
                    photoAttributesObj.href = photoObject.attributes.href;
                }
            }

            if (Object.keys(photoAttributesObj).length) {
                arrayData.push(photoAttributesObj);
            }
        }
    }
    return arrayData;
}

exports.getApi = getApi;
